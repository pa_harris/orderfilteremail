﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using Microsoft.Win32;
using OfficeOpenXml.Style;
using OfficeOpenXml;
using System.IO;
using System.Threading;
using MailKit.Net.Imap;
using MailKit;
using MimeKit;
using System.Text.RegularExpressions;

namespace OrderFilterEmail
{
    public class Model
    {
        public string OrderId { get; set; }
        public string ShippingName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingZipcode { get; set; }
        public string ShippingCountry { get; set; }
        public string ShippingEmail { get; set; }
        public string ProductName { get; set; }
        public string Style { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string Side { get; set; }
        public string Quantity { get; set; }
        public string DesignLink { get; set; }
        public string Total { get; set; }
        public string Tag { get; set; }
        public string Phone { get; set; }
        public string ShippingService { get; set; }
    }
    public static class PropertyCopier<TParent, TChild> where TParent : class
                                            where TChild : class
    {
        public static void Copy(TParent parent, TChild child)
        {
            if (parent != null && child != null)
            {
                var parentProperties = parent.GetType().GetProperties();
                var childProperties = child.GetType().GetProperties();

                foreach (var parentProperty in parentProperties)
                {
                    var isvirtualParent = parentProperty.GetMethod.IsVirtual;
                    if (isvirtualParent)
                        continue;
                    foreach (var childProperty in childProperties)
                    {
                        var isvirtualchildren = childProperty.GetMethod.IsVirtual;
                        if (isvirtualchildren)
                            continue;
                        if (parentProperty.Name == childProperty.Name && parentProperty.PropertyType == childProperty.PropertyType)
                        {
                            try
                            {
                                childProperty.SetValue(child, parentProperty.GetValue(parent));
                            }
                            catch { }
                            break;
                        }
                    }
                }
            }
            else
            {
                return;
            }
        }
    }
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
        #region Properties
        private string _Email = "hyperkick11@gmail.com";
        public string Email { get => _Email; set { _Email = value; OnPropertyChanged(); } }
        private string _Password = "drghyper1763";
        public string Password { get => _Password; set { _Password = value; OnPropertyChanged(); } }
        private DateTime _Date = DateTime.Now;
        public DateTime Date { get => _Date; set { _Date = value; OnPropertyChanged(); } }
        private int _Hour = 1;
        public int Hour { get => _Hour; set { _Hour = value; OnPropertyChanged(); } }

        private List<Model> _Data;
        public List<Model> Data { get => _Data; set { _Data = value; OnPropertyChanged(); } }
        #endregion

        int MaxThread = 10;
        int ThreadCount = 0;

        bool isDone = false;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            Data = new List<Model>();
        }


        private void Filter(object sender, RoutedEventArgs e)
        {
            if (ValidateData())
            {
                Data.Clear();

                Thread t = new Thread(() =>
                {
                    WriteLog("Start...");
                    LoadEmail();
                    WriteLog("Exporting to file...");

                    Export();
                    WriteLog("DONEEEEEEEEEEEEEEEEEE");
                });
                t.IsBackground = true;
                t.Start();

            }
        }

        void WriteLog(string log)
        {
            this.Dispatcher.Invoke(new Action(() =>
            {
                txbLog.Text = log + Environment.NewLine + txbLog.Text;
            }));
        }

        private bool ValidateData()
        {
            if (string.IsNullOrEmpty(Email))
            {
                MessageBox.Show("Email không được trống!", "Cảnh báo");
                return false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                MessageBox.Show("Password không được trống!", "Cảnh báo");
                return false;
            }
            if (Date == null)
            {
                MessageBox.Show("Date không được trống!", "Cảnh báo");
                return false;
            }
            if (Hour > 23 || Hour < 0)
            {
                MessageBox.Show("Hour không hợp lệ! Hour từ 0 đến 23!", "Cảnh báo");
                return false;
            }

            return true;
        }
        private void ExportNew()
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial; // If you use EPPlus in a noncommercial context // according to the Polyform Noncommercial license: 
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            string filePath = "";
            // tạo SaveFileDialog để lưu file excel
            SaveFileDialog dialog = new SaveFileDialog();

            // chỉ lọc ra các file có định dạng Excel
            dialog.Filter = "Excel | *.xlsx | Excel 2003 | *.xls";

            // Nếu mở file và chọn nơi lưu file thành công sẽ lưu đường dẫn lại dùng
            if (dialog.ShowDialog() == true)
            {
                filePath = dialog.FileName;
            }

            // nếu đường dẫn null hoặc rỗng thì báo không hợp lệ và return hàm
            if (string.IsNullOrEmpty(filePath))
            {
                MessageBox.Show("Đường dẫn báo cáo không hợp lệ");
                return;
            }

            try
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    // đặt tên người tạo file
                    p.Workbook.Properties.Author = "Kteam by Harris";

                    // đặt tiêu đề cho file
                    p.Workbook.Properties.Title = "Báo cáo thống kê";

                    //Tạo một sheet để làm việc trên đó
                    p.Workbook.Worksheets.Add("Kteam sheet");

                    // lấy sheet vừa add ra để thao tác
                    ExcelWorksheet ws = p.Workbook.Worksheets.Last();

                    // đặt tên cho sheet
                    ws.Name = "Kteam sheet";
                    // fontsize mặc định cho cả sheet
                    ws.Cells.Style.Font.Size = 11;
                    // font family mặc định cho cả sheet
                    ws.Cells.Style.Font.Name = "Calibri";

                    // Tạo danh sách các column header
                    string[] arrColumnHeader = {
                        "External ID",
                        "Label",
                        "Shipping method",
                        "First name",
                        "Last name",
                        "Email",
                        "Phone",
                        "Country",
                        "Region",
                        "Address line 1",
                        "Address line 2",
                        "City",
                        "Zip",
                        "Quantity",
                        "Print Provider ID",
                        "Blueprint ID",
                        "Variant ID",
                        "Print area back",
                        "Print area front",
                        "Print area neck",
                        "Print area left sleeve",
                        "Print area right sleeve",
                        "Print area neck outer"
                    };

                    // lấy ra số lượng cột cần dùng dựa vào số lượng header
                    var countColHeader = arrColumnHeader.Count();

                    // merge các column lại từ column 1 đến số column header
                    // gán giá trị cho cell vừa merge là Thống kê thông tni User Kteam
                    //ws.Cells[1, 1].Value = "Thống kê thông tin User Kteam";
                    //ws.Cells[1, 1, 1, countColHeader].Merge = true;
                    // in đậm
                    ws.Cells[1, 1, 1, countColHeader].Style.Font.Bold = true;
                    // căn giữa
                    //ws.Cells[1, 1, 1, countColHeader].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int colIndex = 1;
                    int rowIndex = 1;

                    //tạo các header từ column header đã tạo từ bên trên
                    foreach (var item in arrColumnHeader)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];

                        //set màu thành gray
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);

                        //căn chỉnh các border
                        var border = cell.Style.Border;
                        border.Bottom.Style =
                            border.Top.Style =
                            border.Left.Style =
                            border.Right.Style = ExcelBorderStyle.Thin;

                        //gán giá trị
                        cell.Value = item;

                        colIndex++;
                    }

                    // với mỗi item trong danh sách sẽ ghi trên 1 dòng
                    foreach (var item in Data)
                    {
                        // bắt đầu ghi từ cột 1. Excel bắt đầu từ 1 không phải từ 0
                        colIndex = 1;

                        // rowIndex tương ứng từng dòng dữ liệu
                        rowIndex++;

                        //gán giá trị cho từng cell                      
                        ws.Cells[rowIndex, colIndex++].Value = item.OrderId;
                        ws.Cells[rowIndex, colIndex++].Value = item.Tag;
                        ws.Cells[rowIndex, colIndex++].Value = "standard";
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingName.Split(' ').FirstOrDefault();
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingName.Split(' ').LastOrDefault();
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingEmail;
                        ws.Cells[rowIndex, colIndex++].Value = item.Phone;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingCountry;
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingAddress1;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingAddress2;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingCity;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingZipcode;
                        ws.Cells[rowIndex, colIndex++].Value = item.Quantity;
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = item.Style;
                        ws.Cells[rowIndex, colIndex++].Value = item.Color + " " + item.Size;
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                    }

                    //Lưu file lại
                    Byte[] bin = p.GetAsByteArray();
                    File.WriteAllBytes(filePath, bin);
                }
                MessageBox.Show("Xuất excel thành công!");
            }
            catch (Exception EE)
            {
                MessageBox.Show("Có lỗi khi lưu file!");
            }
        }

        private void Export()
        {
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.Commercial; // If you use EPPlus in a noncommercial context // according to the Polyform Noncommercial license: 
            ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;
            string filePath = "";
            // tạo SaveFileDialog để lưu file excel
            SaveFileDialog dialog = new SaveFileDialog();

            // chỉ lọc ra các file có định dạng Excel
            dialog.Filter = "Excel | *.xlsx | Excel 2003 | *.xls";

            // Nếu mở file và chọn nơi lưu file thành công sẽ lưu đường dẫn lại dùng
            if (dialog.ShowDialog() == true)
            {
                filePath = dialog.FileName;
            }

            // nếu đường dẫn null hoặc rỗng thì báo không hợp lệ và return hàm
            if (string.IsNullOrEmpty(filePath))
            {
                MessageBox.Show("Đường dẫn báo cáo không hợp lệ");
                return;
            }

            try
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    // đặt tên người tạo file
                    p.Workbook.Properties.Author = "Kteam by Harris";

                    // đặt tiêu đề cho file
                    p.Workbook.Properties.Title = "Báo cáo thống kê";

                    //Tạo một sheet để làm việc trên đó
                    p.Workbook.Worksheets.Add("Kteam sheet");

                    // lấy sheet vừa add ra để thao tác
                    ExcelWorksheet ws = p.Workbook.Worksheets.Last();

                    // đặt tên cho sheet
                    ws.Name = "Kteam sheet";
                    // fontsize mặc định cho cả sheet
                    ws.Cells.Style.Font.Size = 11;
                    // font family mặc định cho cả sheet
                    ws.Cells.Style.Font.Name = "Calibri";

                    // Tạo danh sách các column header
                    string[] arrColumnHeader = {
                        "Order ID",
                        "Shipping Name",
                        "Shipping Address 1",
                        "Shipping Address 2",
                        "Shipping City",
                        "Shipping State",
                        "Shipping Zipcode",
                        "Shipping Country",
                        "Shipping Phone",
                        "Shipping Email",
                        "Product Name",
                        "Style",
                        "Color",
                        "Size",
                        "Side",
                        "Quantity",
                        "Design Link Front",
                        "Design Link Back",
                        "Shipping Service",
                        "Tag",
                        "Total"
                    };

                    // lấy ra số lượng cột cần dùng dựa vào số lượng header
                    var countColHeader = arrColumnHeader.Count();

                    // merge các column lại từ column 1 đến số column header
                    // gán giá trị cho cell vừa merge là Thống kê thông tni User Kteam
                    //ws.Cells[1, 1].Value = "Thống kê thông tin User Kteam";
                    //ws.Cells[1, 1, 1, countColHeader].Merge = true;
                    // in đậm
                    ws.Cells[1, 1, 1, countColHeader].Style.Font.Bold = true;
                    // căn giữa
                    //ws.Cells[1, 1, 1, countColHeader].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                    int colIndex = 1;
                    int rowIndex = 1;

                    //tạo các header từ column header đã tạo từ bên trên
                    foreach (var item in arrColumnHeader)
                    {
                        var cell = ws.Cells[rowIndex, colIndex];

                        //set màu thành gray
                        var fill = cell.Style.Fill;
                        fill.PatternType = ExcelFillStyle.Solid;
                        fill.BackgroundColor.SetColor(System.Drawing.Color.LightBlue);

                        //căn chỉnh các border
                        var border = cell.Style.Border;
                        border.Bottom.Style =
                            border.Top.Style =
                            border.Left.Style =
                            border.Right.Style = ExcelBorderStyle.Thin;

                        //gán giá trị
                        cell.Value = item;

                        colIndex++;
                    }

                    // với mỗi item trong danh sách sẽ ghi trên 1 dòng
                    foreach (var item in Data)
                    {
                        // bắt đầu ghi từ cột 1. Excel bắt đầu từ 1 không phải từ 0
                        colIndex = 1;

                        // rowIndex tương ứng từng dòng dữ liệu
                        rowIndex++;

                        //gán giá trị cho từng cell                      
                        ws.Cells[rowIndex, colIndex++].Value = item.OrderId;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingName;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingAddress1;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingAddress2;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingCity;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingState;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingZipcode;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingCountry;
                        ws.Cells[rowIndex, colIndex++].Value = item.Phone;
                        ws.Cells[rowIndex, colIndex++].Value = item.ShippingEmail;
                        ws.Cells[rowIndex, colIndex++].Value = item.ProductName;
                        ws.Cells[rowIndex, colIndex++].Value = item.Style;
                        ws.Cells[rowIndex, colIndex++].Value = item.Color;
                        ws.Cells[rowIndex, colIndex++].Value = item.Size;
                        ws.Cells[rowIndex, colIndex++].Value = item.Side;
                        ws.Cells[rowIndex, colIndex++].Value = item.Quantity;
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "";
                        ws.Cells[rowIndex, colIndex++].Value = "standard";
                        ws.Cells[rowIndex, colIndex++].Value = item.Tag;
                        ws.Cells[rowIndex, colIndex++].Value = item.Total;
                    }

                    //Lưu file lại
                    Byte[] bin = p.GetAsByteArray();
                    File.WriteAllBytes(filePath, bin);
                }
                MessageBox.Show("Xuất excel thành công!");
            }
            catch (Exception EE)
            {
                MessageBox.Show("Có lỗi khi lưu file!");
            }
        }
        private void LoadEmail()
        {
            using (var client = new ImapClient())
            {
                try
                {
                    WriteLog("Connecting email...");
                    DateTime inputDate = new DateTime(Date.Year, Date.Month, Date.Day, Hour, 0, 0, DateTimeKind.Local);
                    DateTime newDate = inputDate.AddHours(24);
                    client.Connect("imap.gmail.com", 993, true);
                    client.Authenticate(Email, Password);

                    // The Inbox folder is always available on all IMAP servers...
                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly);
                    inbox.Fetch(new[] { 6987 }, MessageSummaryItems.GMailLabels);

                    if (inbox != null && inbox.Count > 0)
                    {
                        WriteLog($"Lấy được danh sách. Tổng số mail là {inbox.Count}");
                        for (int i = inbox.Count - 1; i >= 0; i--)
                        {
                            var message = inbox.GetMessage(i);
                            if (message.Date.LocalDateTime < inputDate)
                            {
                                isDone = true;
                                break;
                            }
                            if (inputDate < message.Date && message.Date < inputDate.AddHours(24) && message.HtmlBody?.Contains("Your order number is") == true)
                            {
                                while (ThreadCount >= MaxThread)
                                {
                                    Thread.Sleep(2000);
                                }
                                var data = inbox.Fetch(new[] { i }, MessageSummaryItems.GMailLabels).ToList();
                                string gmailLabels = "";
                                foreach (var item in data.FirstOrDefault().GMailLabels)
                                {
                                    gmailLabels += item + "|";
                                }
                                ThreadCount++;
                                Thread t = new Thread(() =>
                                {
                                    WriteLog($"Start {message.Subject} {i} {message.Date.ToLocalTime()}");
                                    FilterEmail(message, gmailLabels);
                                    ThreadCount--;
                                });
                                t.Start();
                                if (i == 0)
                                {
                                    isDone = true;
                                }
                            }
                            else
                            {
                                WriteLog($"Skip {message.Subject} {i} {message.Date.ToLocalTime()}");
                            }
                        }
                    }

                    while (!isDone)
                    {
                        // chờ tới khi i == inbox.Count là xong hết
                    }
                    WriteLog("Donee");
                    client.Disconnect(true);
                }
                catch (Exception ee)
                {
                    MessageBox.Show(ee.Message, "Cảnh báo");
                    WriteLog(ee.Message);
                    return;
                }
            }
        }
        private void FilterEmail(MimeMessage mail, string tag)
        {
            // Tại sao không check null ? Vì nếu null thì cũng lưu null nên check cũng vậy
            Model res = new Model();
            res.Tag = tag;
            string htmlMail = mail.HtmlBody;
            if (string.IsNullOrEmpty(htmlMail))
            {
                WriteLog($"Không lấy được HTML của {mail.Subject}");
                return;
            }

            var orderId = Regex.Match(htmlMail, @"(?<=Your order number is: .*?>).*?(?=<)", RegexOptions.Singleline);
            res.OrderId = orderId.ToString();
            var mailTo = Regex.Match(htmlMail, @"(?<=mailto:).*?(?<="")", RegexOptions.Singleline);
            res.ShippingEmail = mailTo.Value.Replace("\"", "");

            #region Address
            var deliveryAddress = Regex.Match(htmlMail, @"(?<=<address .*?>).*?(?=</address>)", RegexOptions.Singleline);
            var lstAddress = Regex.Matches(deliveryAddress.Value, @"(?=<span .*?>).*?(?=</span>)", RegexOptions.Singleline);
            foreach (var item in lstAddress)
            {
                if (item.ToString().Contains("class=\'name\'"))
                {
                    var name = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingName = name.ToString();
                    continue;
                }
                if (item.ToString().Contains("first-line"))
                {
                    var first = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingAddress1 = first.ToString();
                    continue;
                }
                if (item.ToString().Contains("second-line"))
                {
                    var second = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingAddress2 = second.ToString();
                    continue;
                }
                if (item.ToString().Contains("city"))
                {
                    var city = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingCity = city.ToString();
                    continue;
                }
                if (item.ToString().Contains("state"))
                {
                    var state = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingState = state.ToString();
                    continue;
                }
                if (item.ToString().Contains("zip"))
                {
                    var zip = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    res.ShippingZipcode = zip.ToString();
                    continue;
                }
                if (item.ToString().Contains("country-name"))
                {
                    var country = Regex.Match(item.ToString(), @"(?<=<span .*?>).*?(?=$)", RegexOptions.Singleline);
                    switch (country.Value)
                    {
                        case "United States":
                            res.ShippingCountry = "US";
                            break;
                        case "United Kingdom":
                            res.ShippingCountry = "GB";
                            break;
                        case "Canada":
                            res.ShippingCountry = "CA";
                            break;
                        default:
                            res.ShippingCountry = country.ToString();
                            break;
                    }
                    continue;
                }
            }
            #endregion

            #region Product
            var product = Regex.Matches(htmlMail, @"(?<=<tr>).*?(?=</tr>)", RegexOptions.Singleline);

            foreach (var item in product)
            {
                if (item.ToString().Contains("Order total:"))
                {
                    var total = Regex.Match(item.ToString(), @"(?<=>.*?£).*?(?=<)", RegexOptions.Singleline);
                    res.Total = total.Value;
                    break;
                }
            }

            //var product = Regex.Matches(htmlMail, @"(?<=<td.*?>).*?(?=</td>)", RegexOptions.Singleline);
            foreach (var item in product)
            {
                if (item.ToString().Contains("Style &amp; Size") || item.ToString().Contains("Colour") || item.ToString().Contains("Processing time") || item.ToString().Contains("Color")
                    || item.ToString().Contains("Shop") || item.ToString().Contains("Transaction ID"))
                {
                    //var image = Regex.Match(item.ToString(), @"(?<=<img src="").+?(?<="")", RegexOptions.Singleline);
                    //res.DesignLink = image.Value.Replace("\"", "");

                    var lstProduct = Regex.Matches(item.ToString(), @"(?=<div .*?>).*?(?=</div>)", RegexOptions.Singleline);
                    foreach (var child in lstProduct)
                    {
                        if (child.ToString().Contains("</a>"))
                        {
                            var productName = Regex.Match(child.ToString(), @"(?<=<a .*?>).*?(?=</a>)", RegexOptions.Singleline);
                            res.ProductName = productName.ToString().Trim();
                            continue;
                        }
                        if (child.ToString().Contains("Style &amp; Size"))
                        {
                            var styleAndSize = Regex.Match(child.ToString(), @"(?<=<div .*?>Style &amp; Size: ).*?(?=$)", RegexOptions.Singleline);
                            if (styleAndSize.Value.Contains("-"))
                            {
                                res.Style = styleAndSize.Value.Split('-')[0];
                                res.Size = styleAndSize.Value.Split('-')[1].Trim();
                            }
                            else
                            {
                                try
                                {
                                    string[] styleChar = { "OZ" };
                                    res.Style = styleAndSize.Value.Split(styleChar, StringSplitOptions.RemoveEmptyEntries)[0];
                                    res.Size = styleAndSize.Value.Split(styleChar, StringSplitOptions.RemoveEmptyEntries)[1].Trim();
                                }
                                catch (Exception)
                                {
                                }
                            }
                            continue;
                        }
                        if (child.ToString().Contains("Colour"))
                        {
                            var color = Regex.Match(child.ToString(), @"(?<=<div .*?>Colour: ).*?(?=$)", RegexOptions.Singleline);
                            res.Color = color.Value;
                            continue;
                        }
                        if (child.ToString().Contains("Kleur"))
                        {
                            var color = Regex.Match(child.ToString(), @"(?<=<div .*?>Kleur: ).*?(?=$)", RegexOptions.Singleline);
                            res.Color = color.Value;
                            continue;
                        }
                        if (child.ToString().Contains("Color"))
                        {
                            var color = Regex.Match(child.ToString(), @"(?<=<div .*?>Color: ).*?(?=$)", RegexOptions.Singleline);
                            res.Color = color.Value;
                            continue;
                        }
                        if (child.ToString().Contains("Quantity"))
                        {
                            var quantity = Regex.Match(child.ToString(), @"(?<=<div .*?>.*?Quantity: ).*?(?=$)", RegexOptions.Singleline);
                            res.Quantity = quantity.Value.Trim();
                            continue;
                        }
                        if (child.ToString().Contains("Face mask size"))
                        {
                            var size = Regex.Match(child.ToString(), @"(?<=<div .*?>.*?size: ).*?(?=$)", RegexOptions.Singleline);
                            res.Size = size.Value.Trim();
                            continue;
                        }
                        //if (child.ToString().Contains("Processing time"))
                        //{
                        //    var processingTime = Regex.Match(child.ToString(), @"(?<=<div .*?>.*?time:).*?(?<=days)", RegexOptions.Singleline);
                        //    res.ShippingService = System.Net.WebUtility.HtmlDecode(processingTime.Value.Trim());
                        //    continue;
                        //}
                    }
                    WriteLog(res.ProductName);
                    Model newData = new Model();
                    PropertyCopier<Model, Model>.Copy(res, newData);
                    Data.Add(newData);
                }
            }
            #endregion

            //    #region Design link
            //    //var designLink = Regex.Matches(htmlMail, @"(?<=<img src="".*?).*?(?<="")", RegexOptions.Singleline);
            //    ////var designLink = Regex.Matches(htmlMail, @"(?<=<div class.*?avatar.*?<a .*?>).*?(?<=[?]version)", RegexOptions.Singleline);
            //    //if (orderId.ToString() == "1860362180")
            //    //{

            //    //}
            //    //foreach (var item in designLink)
            //    //{
            //    //    string s = item.ToString().Trim();
            //    //    var s1 = Regex.Match(s, @"(?<=<img src=).*?(?<=\?version)", RegexOptions.Singleline);
            //    //    string link = s1.Value.Replace("?version", "").Replace("\"", "");
            //    //    if (!string.IsNullOrEmpty(res.DesignLink))
            //    //    {
            //    //        res.DesignLink = (link.Contains(res.DesignLink)) ? res.DesignLink : link;
            //    //    }
            //    //    else
            //    //    {
            //    //        res.DesignLink = link;
            //    //    }
            //    //}
            //    #endregion

            //}
        }

        private void ExportNewFormat(object sender, RoutedEventArgs e)
        {
            if (Data != null && Data.Count > 0)
            {
                ExportNew();
            }
            else
            {
                MessageBox.Show("Ấn lọc mail để có dữ liệu export!", "Cảnh báo");
            }
        }
    }
}
